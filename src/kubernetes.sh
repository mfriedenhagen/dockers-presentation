# Create two running nginx instances, which are deployed to listen on port 80
kubectl run mirkos-nginx --image=nginx:latest --replicas=2 --port=80
# Existing objects
kubectl get pods -o wide -Lrun -l run=mirkos-nginx
kubectl get replicasets -o wide -Lrun -l run=mirkos-nginx
kubectl get deployments -o wide -Lrun -l run=mirkos-nginx
# Create service
kubectl expose deployment mirkos-nginx --target-port=80 --type=NodePort
kubectl describe pods,deployments,services -l run=mirkos-nginx
# Shutdown!
kubectl delete deployments,services,pods -l run=mirkos-nginx
